import axios from 'axios';
import { store } from 'react-notifications-component';
import { IAddressViaCep, IData } from '../types';

const connection = axios.create({ baseURL: 'http://localhost:5000' });

const via_cep = axios.create({ baseURL: 'https://viacep.com.br/ws' });

const user_master = {
  email: 'email@email.com',
  password: 'password123'
};

interface IDataRes {
  pages: number;
  data: IData[];
}

const api = {
  login(email: string, password: string) {
    console.log('Simulando login', { email, password });
    try {
      if (email === user_master.email && password === user_master.password) {
        localStorage.setItem('token', 'tokentokentokentoken');
        store.addNotification({
          title: 'Sucesso!',
          message: 'Bem vindo a comunidade Useraid.',
          type: 'success',
          insert: 'top',
          container: 'top-right',
          animationIn: ['animate__animated', 'animate__fadeIn'],
          animationOut: ['animate__animated', 'animate__fadeOut'],
          dismiss: {
            duration: 6000,
            onScreen: true
          }
        });
      } else {
        throw new Error('Falho no login');
      }
    } catch (error) {
      store.addNotification({
        title: 'Falha! :(',
        message: 'Erro no login, por favor revise seus dados.',
        type: 'danger',
        insert: 'top',
        container: 'top-right',
        animationIn: ['animate__animated', 'animate__fadeIn'],
        animationOut: ['animate__animated', 'animate__fadeOut'],
        dismiss: {
          duration: 6000,
          onScreen: true
        }
      });
    }
  },

  fetchUsers(page: number, orderBy: string, search: string): Promise<IDataRes> {
    return new Promise((resolve, reject) => {
      connection
        .get(`/usuarios?q=${search}&_page=${page}&_sort=${orderBy}&_limit=5`)
        .then((res) => {
          console.log(res);
          let links = res.headers.link;
          let arrData = links.split('link:');
          links = arrData.length === 2 ? arrData[1] : links;
          let pages = 1;
          arrData = links.split(',');
          for (let d of arrData) {
            let linkInfo = /<([^>]+)>;\s+rel="([^"]+)"/gi.exec(d);
            if (linkInfo && linkInfo[2] === 'last') {
              const urlParams = new URLSearchParams(linkInfo[1]);
              pages = urlParams.get('_page')
                ? Number(urlParams.get('_page'))
                : 1;
              break;
            }
          }
          resolve({ data: res.data, pages });
        })
        .catch((error) => {
          console.log(error.response);
          store.addNotification({
            title: 'Falha!',
            message: 'Erro ao listar usuários.',
            type: 'danger',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
        });
    });
  },

  deleteUser(id: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      connection
        .delete(`/usuarios/${id}`)
        .then((res) => {
          console.log(res);
          resolve(true);
        })
        .catch((error) => {
          resolve(false);
          console.log(error.response);
          store.addNotification({
            title: 'Falha!',
            message: 'Erro ao deletar usuários.',
            type: 'danger',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
        });
    });
  },

  createUser(data: IData): Promise<boolean> {
    return new Promise((resolve, reject) => {
      connection
        .post(`/usuarios/`, data)
        .then((res) => {
          console.log(res);
          store.addNotification({
            title: 'Sucesso!',
            message: 'Usuário criado com sucesso.',
            type: 'success',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
              duration: 6000,
              onScreen: true
            }
          });
          resolve(true);
        })
        .catch((error) => {
          resolve(false);
          console.log(error.response);
          store.addNotification({
            title: 'Falha!',
            message: 'Falha ao criar usuários.',
            type: 'danger',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
        });
    });
  },

  updateUser(id: string, data: IData): Promise<boolean> {
    return new Promise((resolve, reject) => {
      connection
        .put(`/usuarios/${id}`, data)
        .then((res) => {
          console.log(res);
          store.addNotification({
            title: 'Sucesso!',
            message: 'Usuário criado com sucesso.',
            type: 'success',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
              duration: 6000,
              onScreen: true
            }
          });
          resolve(true);
        })
        .catch((error) => {
          resolve(false);
          console.log(error.response);
          store.addNotification({
            title: 'Falha!',
            message: 'Falha ao criar usuários.',
            type: 'danger',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
        });
    });
  },

  getUserById(id: string): Promise<IData> {
    return new Promise((resolve, reject) => {
      connection
        .get(`/usuarios/${id}`)
        .then((res) => {
          console.log(res);
          resolve(res.data);
        })
        .catch((error) => {
          console.log(error.response);
          store.addNotification({
            title: 'Falha!',
            message: 'Falha ao criar usuários.',
            type: 'danger',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
        });
    });
  },

  getAddresByCep(cep: string): Promise<IAddressViaCep> {
    return new Promise((resolve, reject) => {
      let number_cep = Number(cep.replace('-', ''));
      via_cep
        .get(`/${number_cep}/json`)
        .then((res) => {
          console.log(res);
          resolve(res.data);
        })
        .catch((error) => {
          console.log(error.response);
          store.addNotification({
            title: 'Falha!',
            message: 'Por favor preencha o CEP completamente.',
            type: 'danger',
            insert: 'top',
            container: 'top-right',
            animationIn: ['animate__animated', 'animate__fadeIn'],
            animationOut: ['animate__animated', 'animate__fadeOut'],
            dismiss: {
              duration: 3000,
              onScreen: true
            }
          });
        });
    });
  }
};

export default api;
