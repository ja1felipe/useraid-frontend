const isLogged = () => {
  return localStorage.getItem('token') ? true : false;
};

export { isLogged };
