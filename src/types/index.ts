interface IAddress {
  cep: number;
  rua: string;
  numero: number;
  bairro: string;
  cidade: string;
}

export interface IData {
  id?: number;
  nome: string;
  cpf: string;
  email: string;
  endereco: IAddress;
}

export interface IAddressViaCep {
  cep: string;
  logradouro: string;
  complemento: string;
  bairro: string;
  localidade: string;
  uf: string;
  ibge: string;
  gia: string;
  ddd: string;
  siafi: string;
}
