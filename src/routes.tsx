import React from 'react';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom';
import { isLogged } from './utils/Auth';
const Login = React.lazy(() => import('./pages/Login'));
const Home = React.lazy(() => import('./pages/Home'));

const PrivateRoute: React.FC<{
  component: React.FC;
  path: string;
  exact?: boolean;
}> = (props) => {
  const condition = isLogged();

  return condition ? (
    <Route path={props.path} exact={props.exact} component={props.component} />
  ) : (
    <Redirect to='/login' />
  );
};

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <PrivateRoute path='/' exact component={Home}></PrivateRoute>
        <PrivateRoute path='/user' component={Home}></PrivateRoute>
        <Route path='/login' component={Login}></Route>
      </Switch>
    </BrowserRouter>
  );
}
