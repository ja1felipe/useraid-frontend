const theme = {
  colors: {
    dark: '#0C0910',
    primary: '#861388',
    secondary: '#ea5a04',
    light: '#FCF7F8',
    contrast: '#e0e0e2'
  }
};

export default theme;
