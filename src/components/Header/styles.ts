import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

export const Navbar = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: row;
  align-items: center;

  & button {
    border: 1px solid ${(props) => props.theme.colors.secondary};
    padding: 5px 10px;
  }

  & button li {
    border: none;
    padding: 0;
  }
  & li {
    text-decoration: none;
    padding: 5px 10px;
    text-transform: uppercase;
    margin: 0 10px;
    border: 1px solid ${(props) => props.theme.colors.secondary};
  }

  & a li {
    color: ${(props) => props.theme.colors.primary};
  }

  & a:hover {
    li {
      background: ${(props) => props.theme.colors.secondary + '8c'};
      color: white;
    }
  }
`;
