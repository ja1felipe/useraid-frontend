import React from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import Logo from '../Logo/Logo';

import { Container, Navbar } from './styles';

const Header: React.FC = () => {
  const history = useHistory();

  function handleLogout() {
    localStorage.removeItem('token');
    history.push('/login');
  }
  return (
    <>
      <Container>
        <Logo />
        <Navbar>
          <NavLink to='/'>
            <li>listar usuários</li>
          </NavLink>
          <NavLink to='/user'>
            <li>criar usuário</li>
          </NavLink>
          <a onClick={handleLogout}>
            <li>sair</li>
          </a>
        </Navbar>
      </Container>
      <hr style={{ margin: 0 }} />
    </>
  );
};

export default Header;
