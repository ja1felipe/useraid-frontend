import React from 'react';

import { Container, Title } from './styles';
import { Icon } from '@iconify/react';
import bxRun from '@iconify/icons-bx/bx-run';
import theme from '../../styles/theme';

const Logo: React.FC = () => {
  return (
    <Container>
      <Title>Useraid</Title>
      <Icon
        icon={bxRun}
        style={{ color: theme.colors.secondary, fontSize: '80px' }}
      />
    </Container>
  );
};

export default Logo;
