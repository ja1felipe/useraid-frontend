import React, { ChangeEvent, FormEvent, useState } from 'react';
import validator from 'validator';
import { Container, Input, Form, Button } from './styles';
import api from '../../services/api';
import Logo from '../../components/Logo/Logo';
import { useHistory } from 'react-router-dom';

const Login: React.FC = () => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  const history = useHistory();
  // Simulando login
  async function handleLogin(e: FormEvent) {
    e.preventDefault();
    setLoading(true);
    setTimeout(() => {
      api.login(email, password);
      history.push('/');
      setLoading(false);
    }, 2000);
  }

  return (
    <Container>
      <Logo />
      <Form onSubmit={handleLogin}>
        <Input
          data-testid='email-test'
          type='email'
          value={email}
          onChange={(e: ChangeEvent<HTMLInputElement>) =>
            setEmail(e.target.value)
          }
          placeholder='E-mail'
        />
        <Input
          data-testid='password-test'
          type='password'
          value={password}
          onChange={(e: ChangeEvent<HTMLInputElement>) =>
            setPassword(e.target.value)
          }
          placeholder='Senha'
        />
        <Button
          className={`button ${loading ? 'is-loading' : ''}`}
          disabled={
            password.length < 4 || !validator.isEmail(email) ? true : false
          }
          type='submit'
        >
          Entrar
        </Button>
      </Form>
    </Container>
  );
};

export default Login;
