import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const Input = styled.input`
  padding: 5px;
  border: none;
  border-bottom: 2px solid ${(props) => props.theme.colors.primary + '82'};
  background: none;
  color: ${(props) => props.theme.colors.dark};
  margin-bottom: 25px;
  font-size: 12px;
  width: 220px;
  &:focus {
    border: none;
    outline: none;
    border-bottom: 2px solid ${(props) => props.theme.colors.primary};
  }
`;

export const Form = styled.form`
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const Button = styled.button`
  background: ${(props) => props.theme.colors.secondary + 'e0'};
  color: ${(props) => props.theme.colors.light};
  padding: 10px;
  border: none;
  cursor: pointer;
  text-transform: uppercase;
  font-weight: 600;

  &:disabled {
    background: ${(props) => props.theme.colors.secondary};
  }

  &:hover {
    background: ${(props) => props.theme.colors.secondary};
    color: ${(props) => props.theme.colors.light};
  }
`;
