import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { ThemeProvider } from 'styled-components';
import theme from '../../styles/theme';

import Login from './index';

test('testando inputs', async () => {
  const { getByTestId } = render(
    <ThemeProvider theme={theme}>
      <Login />
    </ThemeProvider>
  );

  await userEvent.type(getByTestId('email-test'), 'jao@jao.com');
  expect(getByTestId('email-test').value).toEqual('jao@jao.com');

  await userEvent.type(getByTestId('password-test'), '1234');
  expect(getByTestId('password-test').value).toEqual('1234');
});
