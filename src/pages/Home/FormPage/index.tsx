import React, {
  ChangeEvent,
  useState,
  FocusEvent,
  FormEvent,
  useRef,
  useEffect
} from 'react';
import { useParams } from 'react-router-dom';
import validator from 'validator';
import api from '../../../services/api';
import { IAddressViaCep, IData } from '../../../types';
import { cepMask, cpfMask } from '../../../utils/Masks';

import { Container, Form, Input, InputGroup, Fields, Button } from './styles';

interface IParams {
  id?: string;
}

const FormPage: React.FC = () => {
  const [nome, setNome] = useState<string>('');
  const [cpf, setCpf] = useState<string>('');
  const [cep, setCep] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [rua, setRua] = useState<string>('');
  const [cidade, setCidade] = useState<string>('');
  const [numero, setNumero] = useState<string>('');
  const [bairro, setBairro] = useState<string>('');

  const [loading, setLoading] = useState<boolean>(false);

  const numberRef = useRef<HTMLInputElement>(null);

  const params = useParams<IParams>();

  useEffect(() => {
    if (params.id) {
      setLoading(true);
      api.getUserById(params.id).then((res: IData) => {
        setNome(res.nome);
        setCpf(res.cpf);
        setCep(cepMask(String(res.endereco.cep)));
        setBairro(res.endereco.bairro);
        setEmail(res.email);
        setRua(res.endereco.rua);
        setNumero(String(res.endereco.numero));
        setCidade(res.endereco.cidade);
        setLoading(false);
      });
    }
  }, [params.id]);

  function validateForm() {
    if (
      nome === '' ||
      cpf === '' ||
      cep === '' ||
      email === '' ||
      rua === '' ||
      cidade === '' ||
      !numero ||
      bairro === ''
    ) {
      return false;
    }

    if (cpf.length < 14 || cep.length < 9) {
      return false;
    }

    if (!validator.isEmail(email)) {
      return false;
    }

    return true;
  }

  function handleSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    setLoading(true);
    let data: IData = {
      nome,
      cpf,
      email,
      endereco: {
        cep: Number(cep.replace('-', '')),
        rua,
        numero: Number(numero),
        bairro,
        cidade
      }
    };
    if (!params.id) {
      api.createUser(data).then((res) => {
        if (res) {
          setLoading(false);
          clearFields();
        }
      });
    } else {
      api.updateUser(params.id, data).then((res) => {
        if (res) {
          setLoading(false);
          clearFields();
        }
      });
    }
  }

  function clearFields() {
    setNome('');
    setEmail('');
    setCidade('');
    setCep('');
    setCpf('');
    setNumero('');
    setBairro('');
    setRua('');
  }

  return (
    <Container>
      <Form
        onSubmit={(e: FormEvent<HTMLFormElement>) => {
          handleSubmit(e);
        }}
      >
        {params.id ? (
          <h1>Editar usuário</h1>
        ) : (
          <h1>Cadastro de novo usuário</h1>
        )}
        <Fields>
          <InputGroup>
            <label htmlFor='nome'>Nome</label>
            <Input
              id='nome'
              value={nome}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setNome(e.target.value)
              }
              placeholder='ex: João Paulo'
              type='text'
            />
          </InputGroup>
          <InputGroup>
            <label htmlFor='cpf'>CPF</label>
            <Input
              id='cpf'
              value={cpf}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setCpf(cpfMask(e.target.value))
              }
              placeholder='ex: 123.123.123-12'
              type='text'
            />
          </InputGroup>
          <InputGroup>
            <label htmlFor='email'>E-mail</label>
            <Input
              id='email'
              value={email}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setEmail(e.target.value)
              }
              placeholder='ex: joao@email.com'
              type='email'
            />
          </InputGroup>
          <InputGroup>
            <label htmlFor='cep'>CEP</label>
            <Input
              id='cep'
              value={cep}
              placeholder='ex: 12312-123'
              onBlur={(e: FocusEvent<HTMLInputElement>) => {
                api
                  .getAddresByCep(e.target.value)
                  .then((res: IAddressViaCep) => {
                    setBairro(res.bairro);
                    setRua(res.logradouro);
                    setCidade(res.localidade);
                    if (numberRef.current) numberRef.current.focus();
                  });
              }}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setCep(cepMask(e.target.value))
              }
              type='text'
            />
          </InputGroup>
          <InputGroup>
            <label htmlFor='rua'>Rua</label>
            <Input
              id='rua'
              value={rua}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setRua(e.target.value)
              }
              placeholder='ex: Rua Lisboa'
              type='text'
            />
          </InputGroup>
          <InputGroup>
            <label htmlFor='numero'>Número</label>
            <Input
              id='numero'
              ref={numberRef}
              value={numero}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setNumero(e.target.value)
              }
              placeholder='ex: 273'
              type='text'
            />
          </InputGroup>
          <InputGroup>
            <label htmlFor='bairro'>Bairro</label>
            <Input
              id='bairro'
              value={bairro}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setBairro(e.target.value)
              }
              placeholder='ex: Centro'
              type='text'
            />
          </InputGroup>
          <InputGroup>
            <label htmlFor='cidade'>Cidade</label>
            <Input
              id='cidade'
              value={cidade}
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setCidade(e.target.value)
              }
              placeholder='ex: São Paulo'
              type='text'
            />
          </InputGroup>
        </Fields>
        <Button
          type='submit'
          disabled={!validateForm()}
          title={
            validateForm()
              ? ''
              : 'Por favor preencha todos os campos corretamente.'
          }
          className={`button ${loading ? 'is-loading' : ''}`}
        >
          {params.id ? 'Editar' : 'Criar'}
        </Button>
      </Form>
    </Container>
  );
};

export default FormPage;
