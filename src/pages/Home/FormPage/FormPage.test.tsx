import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ThemeProvider } from 'styled-components';
import theme from '../../../styles/theme';
import { createMemoryHistory } from 'history';
import React from 'react';
import { Router } from 'react-router-dom';

import FormPage from './index';

test('testando renderização', async () => {
  const history = createMemoryHistory();
  const { getByText, getByLabelText } = render(
    <Router history={history}>
      <ThemeProvider theme={theme}>
        <FormPage />
      </ThemeProvider>
    </Router>
  );

  expect(getByText('Cadastro de novo usuário')).not.toBeNull();

  getByLabelText('Nome');
  getByLabelText('CPF');
  getByLabelText('E-mail');
  getByLabelText('CEP');
  getByLabelText('Rua');
  getByLabelText('Número');
  getByLabelText('Bairro');
  getByLabelText('Cidade');
});

test('testando inputs', async () => {
  const history = createMemoryHistory();
  const { getByText, getByLabelText } = render(
    <Router history={history}>
      <ThemeProvider theme={theme}>
        <FormPage />
      </ThemeProvider>
    </Router>
  );

  await userEvent.type(getByLabelText('Nome'), 'Felipe');
  expect(getByLabelText('Nome').value).toEqual('Felipe');

  await userEvent.type(getByLabelText('CPF'), '123.123.123-12');
  expect(getByLabelText('CPF').value).toEqual('123.123.123-12');

  await userEvent.type(getByLabelText('E-mail'), 'jao@jao.com');
  expect(getByLabelText('E-mail').value).toEqual('jao@jao.com');

  await userEvent.type(getByLabelText('CEP'), '12312-31');
  expect(getByLabelText('CEP').value).toEqual('12312-31');

  await userEvent.type(getByLabelText('Rua'), 'rua rua');
  expect(getByLabelText('Rua').value).toEqual('rua rua');

  await userEvent.type(getByLabelText('Número'), '2');
  expect(getByLabelText('Número').value).toEqual('2');

  await userEvent.type(getByLabelText('Bairro'), 'bairro');
  expect(getByLabelText('Bairro').value).toEqual('bairro');

  await userEvent.type(getByLabelText('Cidade'), 'city');
  expect(getByLabelText('Cidade').value).toEqual('city');
});
