import React from 'react';
import { Route, withRouter } from 'react-router-dom';
import Header from '../../components/Header';
import FormPage from './FormPage';
import TablePage from './TablePage';

const Home: React.FC = () => {
  return (
    <div className='container'>
      <Header />
      <Route path='/' exact component={TablePage}></Route>
      <Route path='/user/' exact component={FormPage}></Route>
      <Route path='/user/:id' component={FormPage}></Route>
    </div>
  );
};

export default withRouter(Home);
