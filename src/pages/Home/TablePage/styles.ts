import styled from 'styled-components';

export const TableBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 20px;
`;

export const SearchBox = styled.form`
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  & > button {
    color: ${(props) => props.theme.colors.light};
    background: ${(props) => props.theme.colors.primary};
    height: 30px;
    font-size: 13px;
    margin-left: 5px;
  }
`;

export const PaginationBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  & button,
  & select {
    margin: 5px;
    height: 30px !important;
  }
`;
