import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react';

import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table';
import api from '../../../services/api';
import { IData } from '../../../types';
import { Icon } from '@iconify/react';
import bxSearchAlt from '@iconify/icons-bx/bx-search-alt';
import { TableBox, SearchBox, PaginationBox } from './styles';
import theme from '../../../styles/theme';

const TablePage: React.FC = () => {
  const [data, setData] = useState<IData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [pages, setPages] = useState<number>(1);
  const [page, setPage] = useState<number>(1);
  const [orderBy, setOrderBy] = useState<string>('');
  const [search, setSearch] = useState<string>('');
  const [searchInput, setSearchInput] = useState<string>('');
  const [refresh, setRefresh] = useState<boolean>(false);

  const columns = [
    {
      name: 'Nome',
      sortable: true,
      selector: 'nome'
    },
    {
      name: 'CPF',
      sortable: true,
      selector: 'cpf'
    },
    {
      name: 'E-mail',
      sortable: true,
      selector: 'email'
    },
    {
      name: 'Cidade',
      sortable: true,
      selector: 'endereco.cidade'
    }
  ];

  useEffect(() => {
    setLoading(true);
    api.fetchUsers(page, orderBy, search).then((res) => {
      setData(res.data);
      setPages(res.pages);
      setLoading(false);
    });
  }, [page, orderBy, search, refresh]);
  return (
    <>
      {data.length > 0 && (
        <SearchBox
          onSubmit={(e: FormEvent<HTMLFormElement>) => {
            e.preventDefault();
            setSearch(searchInput);
            setPage(1);
          }}
        >
          <div className='control has-icons-right'>
            <input
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setSearchInput(e.target.value)
              }
              value={searchInput}
              className='input is-small'
              type='text'
              placeholder='Buscar por nome'
            />
            <span className='icon is-small is-right'>
              <Icon
                icon={bxSearchAlt}
                style={{ color: theme.colors.contrast, fontSize: '20px' }}
              />
            </span>
          </div>
          <button
            onClick={() => {
              setPage(1);
              setSearch(searchInput);
            }}
            type='submit'
            className='button'
          >
            Buscar
          </button>
        </SearchBox>
      )}
      {loading ? (
        <Spinner />
      ) : data.length > 0 ? (
        <TableBox>
          <Table
            refresh={setRefresh}
            orderBy={orderBy}
            setSort={setOrderBy}
            columns={columns}
            data={data}
          />
        </TableBox>
      ) : (
        <div style={{ textAlign: 'center', marginTop: 20, fontSize: 25 }}>
          Nenhum usuário cadastrado ainda, aproveite e cadastre um novo :D
        </div>
      )}
      {data.length > 0 && (
        <PaginationBox>
          <button
            onClick={() => setPage((prev) => prev - 1)}
            className={`button`}
            disabled={page < 2 ? true : false}
          >
            Anterior
          </button>
          <select
            className='select'
            value={page}
            onChange={(e: ChangeEvent<HTMLSelectElement>) =>
              setPage(Number(e.target.value))
            }
          >
            {Array.from(Array(pages), (e, i) => {
              return (
                <option key={i} value={i + 1}>
                  {i + 1}
                </option>
              );
            })}
          </select>
          <button
            disabled={page === pages ? true : false}
            onClick={() => setPage((prev) => prev + 1)}
            className='button'
          >
            Próximo
          </button>
        </PaginationBox>
      )}
    </>
  );
};

export default TablePage;
